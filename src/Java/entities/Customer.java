/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import structures.UserInfo;

/**
 *
 * @author grp3
 */
@Entity
@NamedQueries({
    
    @NamedQuery(name = "Login.control", query = "SELECT l FROM Customer l WHERE l.uname = :uname")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Embedded
    private UserInfo customer;
    private String IName;
    private String uname;
    private String password;
    
    private Bid bid;
    /* NOT IMPLEMENTED YET (AUCTION)
    private List<Auction> ownedAuctions;
    private List<Auction> biddefAuctions;
    */

    public Customer() {
       this.customer = new UserInfo();
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public UserInfo getUser() {
        return customer;
    }

    public void setUser(UserInfo user) {
        this.customer = user;
    }

    public String getIName() {
        return IName;
    }

    public void setIName(String IName) {
        this.IName = IName;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    /* NOT IMPLEMENTED YET
    public <Auction> getOwnedAuctions() {
        return ownedAuctions;
    }

    public void setOwnedAuctions(<Auction> ownedAuctions) {
        this.ownedAuctions = ownedAuctions;
    }

    public <Auction> getBiddefAuctions() {
        return biddefAuctions;
    }

    public void setBiddefAuctions(<Auction> biddefAuctions) {
        this.biddefAuctions = biddefAuctions;
    }*/
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.customer);
        hash = 53 * hash + Objects.hashCode(this.IName);
        hash = 53 * hash + Objects.hashCode(this.uname);
        hash = 53 * hash + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (!Objects.equals(this.IName, other.IName)) {
            return false;
        }
        if (!Objects.equals(this.uname, other.uname)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.customer, other.customer)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", user=" + customer + ", IName=" + IName + ", uname=" + uname + '}';
    }
    

    
    
}
