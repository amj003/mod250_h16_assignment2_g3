/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.ProductFacade;
import entities.Product;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
/**
 *
 * @author Igor
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@Named(value = "productView")
@RequestScoped
public class ProductView {
    List<Product> allProducts;
    Product  product;
    @EJB
    ProductFacade productFacade;
    
    public ProductView() {
        this.product = new Product();
    }
    
    public String postProduct()
    {
        this.productFacade.create(product);
        return "/product/view";
    }
    public String findProductByIndex(Long id){
        this.productFacade.find(id);
       return "index";
    }
    
    public List<Product> getAllProducts(){
        return this.productFacade.findAll();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    

    /**
     * Creates a new instance of ProductView
     */
    
}

