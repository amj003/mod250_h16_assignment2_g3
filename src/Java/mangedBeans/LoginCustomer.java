/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.CustomerFacade;
import entities.Customer;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Igor
 */
@ManagedBean
@Named(value = "loginCustomer")
@SessionScoped
public class LoginCustomer {
     private Customer customer;
     @EJB
     private CustomerFacade customerFacade;
     private boolean isLoggedIn;
     private String userName;

    

    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }
    public void logOut()
    {
        isLoggedIn = false;
        
    }
    
    private String pasword;
    public LoginCustomer() {
        this.customer = new Customer();
        isLoggedIn = false;
    }
    
    
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    
    public String loginControl()
    {
        System.out.println("mangedBeans.LoginCustomer.loginControl()");
        Customer cust = this.customerFacade.findCustomerByUName(userName);
        if (Objects.nonNull(cust) && cust.getUname().equals(userName)&&cust.getPassword().equals(pasword)) {
            isLoggedIn = true;
        return "/index";
        }
        
        return "/customer/create";
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }
    public String login(){
        if((userName==null) || (userName.length()<1))
                return "failure";
        if((pasword==null) || (pasword.length()<1))
            return "failure";
        
        
        
            
            
        if(userName.equals("test")&&pasword.equals("test"))
            return "succes";
        else
            return "failure";
    }
   
    
}
