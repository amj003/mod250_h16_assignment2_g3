/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.CustomerFacade;
import entities.Customer;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Igor
 */
@ManagedBean
@Named(value = "customerView")
@RequestScoped
public class CustomerView {
    
    Customer customer;
    
    @EJB
    CustomerFacade customerFacade;
    
    public CustomerView() {
        customer = new Customer();
    }
    
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public String registerUser()
    {
        this.customerFacade.create(customer);
        return "/index";
    }
    

    /**
     * Creates a new instance of CustomerView
     */
    
    
}
