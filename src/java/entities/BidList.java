/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author grp3
 */
@Entity
public class BidList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private List<Bid> bids;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Add a bid to the bidList
     * @param bid added bid
     */
    public void addBid(Bid bid){
        bids.add(bid);
    }
    /**
     * @param bidId 
     * @return bid related to the bidID
     */
    public Bid getBid(int bidId){
        return bids.get(bidId);
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }
    /**
     * Called from JSF
     * @param value to validate
     * @throws ValidatorException if input is incorrect 
     */
    private void validateBid(FacesContext context,
                            UIComponent component,
                            Object value) throws ValidatorException{
        if(value == null)
            return;
        String bid = (String)value;
        float currentBid;
        FacesMessage message;
        try{
            currentBid = Float.parseFloat(bid);
        }
        catch(NumberFormatException e){
            message = new FacesMessage("Wrong data input");
            throw new ValidatorException(message);
        }
        for(Bid b : bids){
                if(b.getBidValue()>=currentBid){
                    message = new FacesMessage("Curret bid is lower or equals to the highest bid");
                    throw new ValidatorException(message);
                }
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.bids);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BidList other = (BidList) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.bids, other.bids)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BidList{" + "id=" + id + ", bids=" + bids + '}';
    }
    


    
}
