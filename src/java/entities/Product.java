/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import Interfaces.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import structures.Category;

/**
 *
 * @author grp3
 */
@Entity
@Table(name = "PRODUCT")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String productName;
    private String pictureURL;
    @Column(name = "PR_PRODDESC")
    private String productDescription;
    
    private Features features;
    @Enumerated(EnumType.STRING)
    Category category;
    
    public List<Category> getAllCategories(){
        
    return Arrays.asList(category.values());
    }
    

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getProductName(){
        return this.productName;
    }
    public void setProductName(String name){
        this.productName = name;
    }
    public String getPictureURL(){
        return this.pictureURL;
    }
    public void setPictureUrl(String picture){
        this.pictureURL = picture;
    }
    public String getProductDescription(){
        return this.productDescription;
    }
    public void setProductDescription(String description){
        this.productDescription = description;
    }
    public Features getFeatures(){
        return this.features;
    }
    public void setFeature(Features features){
        this.features = features;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.productName);
        hash = 23 * hash + Objects.hashCode(this.pictureURL);
        hash = 23 * hash + Objects.hashCode(this.productDescription);
        hash = 23 * hash + Objects.hashCode(this.features);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (!Objects.equals(this.productName, other.productName)) {
            return false;
        }
        if (!Objects.equals(this.pictureURL, other.pictureURL)) {
            return false;
        }
        if (!Objects.equals(this.productDescription, other.productDescription)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", productName=" + productName + ", pictureURL=" + pictureURL + ", productDescription=" + productDescription + ", features=" + features + '}';
    }

    

    
    
}
